package pl.edu.agh.kis.erudite.review;

import javax.persistence.*;

@Entity
@Table
public class Review {
    @Column(name = "id_review")
    private @Id
    @GeneratedValue Long id;
    private String content;

    public Review() {}

}
