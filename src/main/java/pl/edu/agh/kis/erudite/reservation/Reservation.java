package pl.edu.agh.kis.erudite.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.agh.kis.erudite.catalog.Catalog;
import pl.edu.agh.kis.erudite.edition.Edition;
import pl.edu.agh.kis.erudite.user.User;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Reservation {
    @Column(name = "id_reservation")
    private @Id
    @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "reservation_user_fk"))
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_edition", foreignKey = @ForeignKey(name = "reservation_edition_fk"), nullable = false)
    private Edition edition;

    @Column(nullable = false)
    private LocalDate submitted;
    private LocalDate receiptTerm;

    public Reservation(User user, Edition edition, LocalDate submitted) {
        this.user = user;
        this.edition = edition;
        this.submitted = submitted;
    }
}
