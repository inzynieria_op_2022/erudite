package pl.edu.agh.kis.erudite.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tags")
@CrossOrigin(origins = "http://localhost:3000")
public class TagController {
    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService){
        this.tagService = tagService;
    }

    @GetMapping("/{id}")
    public Tag get(@PathVariable Long id) {
        return tagService.getTag(id);
    }

    @GetMapping
    public Iterable<Tag> getAll() {
        return tagService.getAllTags();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Tag tag) {
        tagService.addNewTag(tag);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id")Long id) {
        tagService.deleteTag(id);
    }
}
