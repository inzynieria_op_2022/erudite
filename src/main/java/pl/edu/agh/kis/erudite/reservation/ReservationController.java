package pl.edu.agh.kis.erudite.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.kis.erudite.edition.Edition;
import pl.edu.agh.kis.erudite.edition.EditionService;
import pl.edu.agh.kis.erudite.user.User;
import pl.edu.agh.kis.erudite.user.UserService;

import java.util.List;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reservations")
@CrossOrigin(origins = "http://localhost:3000")
public class ReservationController {
    private ReservationService service;
    private UserService userService;
    private EditionService editionService;

    @GetMapping("/{id}")
    public Reservation get(@PathVariable("id") Long id) {
        return service.getReservation(id);
    }

    @GetMapping("/getForUser/{userId}")
    public List<Reservation> getUserReservations(@PathVariable Long userId, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication;
        User user = userService.getUserByEmail(userDetails.getUsername());
        //if (user.getId() != userId)
            //throw new UserNotAuthorizedException();
        return service.getReservationsForUser(userId);
    }

    @PostMapping("/{editionId}/{userId}")
    public void addReservation(@PathVariable Long editionId, @PathVariable Long userId, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication;
        //User user = userService.getUserByEmail(userDetails.getUsername());
        User user = userService.getUser(userId);
        Edition edition = editionService.getEdition(editionId);
        service.reserve(edition, user);
    }
}
