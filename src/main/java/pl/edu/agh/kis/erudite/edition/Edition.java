package pl.edu.agh.kis.erudite.edition;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.agh.kis.erudite.author.Author;
import pl.edu.agh.kis.erudite.book.Book;
import pl.edu.agh.kis.erudite.publisher.Publisher;

import javax.persistence.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Edition {
    @Column(name = "id_edition")
    private @Id
    @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn(name = "id_book", foreignKey = @ForeignKey(name = "edition_book_fk"))
    private Book book;

    @ManyToOne
    @JoinColumn(name = "id_publisher", foreignKey = @ForeignKey(name = "edition_publisher_fk"))
    private Publisher publisher;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Author> authors = new ArrayList<>();

    @Column(nullable = false)
    private String isbn;
    @Column(length = 1000)
    private String description;
    @Column(name = "year_")
    private int year;
    private int pages;
    private String cover;

    public Edition(Book book, String isbn, Author author) {
        this.book = book;
        this.isbn = isbn;
        this.authors.add(author);
    }

    public Edition(Book book, String isbn, Author author, Author author2) {
        this.book = book;
        this.isbn = isbn;
        this.authors.add(author);
        this.authors.add(author2);
    }
}
