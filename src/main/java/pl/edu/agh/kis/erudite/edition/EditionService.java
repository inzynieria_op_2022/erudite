package pl.edu.agh.kis.erudite.edition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

import java.util.List;
import java.util.Locale;

@Service
public class EditionService {
    public final EditionRepository repository;

    public List<Edition> searchEditions(String title, String author, String isbn) {
        if (title != null) {
            title = title.toUpperCase();
        }
        if (author != null) {
            author = author.toUpperCase();
        }
        return repository.findByTitleAuthorAndIsbn(title, author, isbn);
    }
    @Autowired
    public EditionService(EditionRepository repository) {
        this.repository = repository;
    }

    public Edition getEdition(Long id) {
        return repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public Edition addEdition(Edition edition) {
        return repository.save(edition);
    }

    public void deleteEdition(Long id) {

        repository.deleteById(id);
    }

    public Iterable<Edition> getAllEditions() {
        return repository.findAll();
    }
}
