package pl.edu.agh.kis.erudite.bookTag;

import javax.persistence.*;

@Entity
@Table
public class BookTag {
    @Column(name = "id_book_tag")
    private @Id
    @GeneratedValue Long id;

    public BookTag() {}
}
