package pl.edu.agh.kis.erudite.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/publishers")
@CrossOrigin(origins = "http://localhost:3000")
public class PublisherController {
    private final PublisherService publisherService;

    @Autowired
    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping
    public Iterable<Publisher> getAllPublishers() {
        return publisherService.getAllPublishers();
    }

    @GetMapping("/{id}")
    public Publisher getPublisher(@PathVariable Long id) {
        return publisherService.getPublisher(id);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createPublisher(@RequestBody Publisher publisher) {
        publisherService.createPublisher(publisher);
    }

    @DeleteMapping("{id}")
    public void deletePublisher(@PathVariable("id") Long id) {
        publisherService.deletePublisher(id);
    }
}
