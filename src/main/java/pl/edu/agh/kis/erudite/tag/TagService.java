package pl.edu.agh.kis.erudite.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class TagService {
    private final TagRepository tagRepository;

    @Autowired
    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public Iterable<Tag> getAllTags() {
        return tagRepository.findAll();
    }

    public Tag getTag(Long id) {
        return tagRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void addNewTag(Tag tag) {
        tagRepository.save(tag);
    }

    public void deleteTag(Long tagId) {
        boolean exists = tagRepository.existsById(tagId);
        if (!exists) {
            throw new ItemNotFoundException(tagId);
        }
        tagRepository.deleteById(tagId);
    }
}
