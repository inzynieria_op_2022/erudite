package pl.edu.agh.kis.erudite.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/catalog")
@CrossOrigin(origins = "http://localhost:3000")
public class CatalogController {
    private final CatalogService catalogService;

    @Autowired
    public CatalogController(CatalogService catalogService){
        this.catalogService = catalogService;
    }

    @GetMapping("/{id}")
    public Catalog get(@PathVariable Long id) {
        return catalogService.getCatalogComponent(id);
    }

    @GetMapping
    public Iterable<Catalog> getAll() {
        return catalogService.getCatalog();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody Catalog catalog) {
        catalogService.addNewCatalogComponent(catalog);
    }

    @DeleteMapping( "/{id}")
    public void delete(@PathVariable Long id) {
        catalogService.deleteCatalogComponent(id);
    }
}
