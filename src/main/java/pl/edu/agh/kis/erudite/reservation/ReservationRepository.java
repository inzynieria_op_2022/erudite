package pl.edu.agh.kis.erudite.reservation;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    Optional<Reservation> findByEditionId(Long id);
    List<Reservation> findByUserId(Long id);
}
