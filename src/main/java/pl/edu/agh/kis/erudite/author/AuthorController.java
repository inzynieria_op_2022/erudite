package pl.edu.agh.kis.erudite.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/authors")
public class AuthorController {
    private AuthorService service;
    @Autowired
    public AuthorController(AuthorService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public Author get(@PathVariable Long id) {
        return service.getAuthor(id);
    }

    @PostMapping
    public Author add(@RequestBody @Validated Author author) {
        return service.addAuthor(author);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.deleteAuthor(id);
    }
}
