package pl.edu.agh.kis.erudite.loan;


import javax.persistence.*;

import java.time.LocalDate;
import java.util.Objects;


import pl.edu.agh.kis.erudite.catalog.Catalog;
import pl.edu.agh.kis.erudite.user.User;

@Entity
@Table(name = "loans")
public class Loan {
    @Column(name = "id_loan")
    private @Id
    @GeneratedValue Long id;

    @ManyToOne
    @JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "loan_user_fk"))
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_catalog", foreignKey = @ForeignKey(name = "loan_catalog_fk"))
    private Catalog catalog;
    private LocalDate date_of_loan;
    private LocalDate date_of_return;
    private int prolongation;





    public Loan() {}

    public Loan(Long id, LocalDate loanDate, LocalDate returnDate, int prolongation, User user, Catalog catalog) {
        this.id = id;
        this.date_of_loan = loanDate;
        this.date_of_return = returnDate;
        this.prolongation = prolongation;
        this.user = user;
        this.catalog = catalog;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate_of_loan() {
        return date_of_loan;
    }

    public void setDate_of_loan(LocalDate loanDate) {
        this.date_of_loan = loanDate;
    }

    public LocalDate getDate_of_return() {
        return date_of_return;
    }

    public void setDate_of_return(LocalDate returnDate) {
        this.date_of_return = returnDate;
    }

    public int getProlongation() {
        return prolongation;
    }

    public void setProlongation(int prolongation) {
        this.prolongation = prolongation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return prolongation == loan.prolongation && Objects.equals(id, loan.id) && Objects.equals(date_of_loan, loan.date_of_loan) &&
                Objects.equals(date_of_return, loan.date_of_return) && Objects.equals(user, loan.user) && Objects.equals(catalog, loan.catalog);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date_of_loan, date_of_return, prolongation, user, catalog);
    }

    @Override
    public String toString() {
        return "Loans{" +
                "id=" + id +
                ", loanDate=" + date_of_loan +
                ", returnDate=" + date_of_return +
                ", prolongation=" + prolongation +
                ", user=" + user +
                ", catalog=" + catalog +
                '}';
    }
}
