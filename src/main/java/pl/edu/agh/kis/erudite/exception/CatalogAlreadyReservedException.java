package pl.edu.agh.kis.erudite.exception;

public class CatalogAlreadyReservedException extends RuntimeException {
    public CatalogAlreadyReservedException() { super("Can't reserve catalog that is already reserved"); }
}
