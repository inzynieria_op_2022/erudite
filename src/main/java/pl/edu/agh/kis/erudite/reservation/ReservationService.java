package pl.edu.agh.kis.erudite.reservation;

import org.springframework.beans.CachedIntrospectionResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.catalog.Catalog;
import pl.edu.agh.kis.erudite.catalog.CatalogRepository;
import pl.edu.agh.kis.erudite.edition.Edition;
import pl.edu.agh.kis.erudite.exception.CatalogAlreadyReservedException;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;
import pl.edu.agh.kis.erudite.user.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    private ReservationRepository repository;
    @Autowired
    public ReservationService(ReservationRepository repository) {
        this.repository = repository;
    }
    public Reservation getReservation(Long id) {
        return repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void reserve(Edition edition, User user) {
        Optional<Reservation> existingReservation = repository.findByEditionId(edition.getId());
        if (existingReservation.isPresent())
            throw new CatalogAlreadyReservedException();
        Reservation reservation = new Reservation(user, edition, LocalDate.now());
        repository.save(reservation);
    }

    public List<Reservation> getReservationsForUser(Long userId) {
        return repository.findByUserId(userId);
    }
}
