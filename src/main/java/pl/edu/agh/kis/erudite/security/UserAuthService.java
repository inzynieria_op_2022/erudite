package pl.edu.agh.kis.erudite.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import pl.edu.agh.kis.erudite.role.Role;
import pl.edu.agh.kis.erudite.role.RoleEnum;
import pl.edu.agh.kis.erudite.role.RoleRepository;
import pl.edu.agh.kis.erudite.user.User;
import pl.edu.agh.kis.erudite.user.UserRepository;

import javax.transaction.Transactional;
import java.util.Arrays;

@Service
@Transactional
public class UserAuthService {
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;
    @Autowired
    public UserAuthService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public void registerReturn(Model model)
    {
        model.addAttribute("user", new UserDto());
    }

    public void registerUser(UserDto userDto) {
        if (userRepository.findByEmail(userDto.getEmail()) != null) {
            throw new IllegalArgumentException("User with this email already exists");
        }
        User user = new User();
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());

        Role role = roleRepository.findByName("library_user");
        user.setRoles(Arrays.asList(role));

        userRepository.save(user);
    }

    private void setupRoles(){
        //for now when sql file is not working
        Role role = new Role();
        Role role2 = new Role();
        Role role3 = new Role();
        role.setName(RoleEnum.library_user.toString());
        role2.setName(RoleEnum.librarian.toString());
        role3.setName(RoleEnum.admin.toString());
        roleRepository.save(role);
        roleRepository.save(role2);
        roleRepository.save(role3);
    }

    boolean emailExists(String email) {
        return userRepository.findByEmail(email) != null;
    }

}
