package pl.edu.agh.kis.erudite.catalog;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table
public class Catalog {
    @Column(name = "id_catalog")
    private @Id
    @GeneratedValue Long id;
    private Long edition_id;
    private String shelf_mark;

    public Catalog() {}

    public Catalog(Long id, Long edition_id, String shelf_mark) {
        this.id = id;
        this.edition_id = edition_id;
        this.shelf_mark = shelf_mark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEdition_id() {
        return edition_id;
    }

    public void setEdition_id(Long edition_id) {
        this.edition_id = edition_id;
    }

    public String getShelf_mark() {
        return shelf_mark;
    }

    public void setShelf_mark(String shelf_mark) {
        this.shelf_mark = shelf_mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Catalog catalog = (Catalog) o;

        return Objects.equals(id, catalog.id) && Objects.equals(edition_id, catalog.edition_id) && Objects.equals(shelf_mark, catalog.shelf_mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, edition_id, shelf_mark);
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "id=" + id +
                ", edition_id=" + edition_id +
                ", shelf_mark='" + shelf_mark + '\'' +
                '}';
    }
}
