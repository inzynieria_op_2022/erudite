package pl.edu.agh.kis.erudite.exception;

public class UserNotAuthorizedException extends RuntimeException {
    public UserNotAuthorizedException() { super("Not authorized to search for this user."); }
}
