package pl.edu.agh.kis.erudite.loan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loans")
@CrossOrigin(origins = "http://localhost:3000")
public class LoanController {
    private final LoanService loanService;

    @Autowired
    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @GetMapping("/{id}")
    public Loan getLoan(@PathVariable Long id) {
        return loanService.getLoan(id);
    }

    @GetMapping
    public Iterable<Loan> getAllLoans() {
        return loanService.getAllLoans();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createLoan(@RequestBody Loan loan) {
        loanService.createLoan(loan);
    }

    @DeleteMapping("/{id}")
    public void deleteLoan(@PathVariable("id") Long id) {
        loanService.deleteLoan(id);
    }





}
