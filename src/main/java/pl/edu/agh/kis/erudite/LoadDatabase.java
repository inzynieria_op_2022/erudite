package pl.edu.agh.kis.erudite;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.agh.kis.erudite.author.Author;
import pl.edu.agh.kis.erudite.book.Book;
import pl.edu.agh.kis.erudite.book.BookRepository;
import pl.edu.agh.kis.erudite.edition.Edition;
import pl.edu.agh.kis.erudite.edition.EditionRepository;

@Configuration
public class LoadDatabase {
    @Bean
    CommandLineRunner initDatabase(BookRepository bookRepository, EditionRepository editionRepository) {
        return args -> {
            Book book = new Book("Crime and Punishment", 1866);
            Author author = new Author("Fyodor Dostoevsky");
            bookRepository.save(book);
            editionRepository.save(new Edition(book, "12312312313", author));

            author = new Author("Fyodor Dostoevsky");
            book = new Book("In crime scarlet", 1876);
            Author author2 = new Author("Ktos tam");
            bookRepository.save(book);
            editionRepository.save(new Edition(book, "9866712314", author, author2));
        };
    }
}
