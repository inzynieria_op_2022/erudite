package pl.edu.agh.kis.erudite.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserAuthController {
    private final UserAuthService userAuthService;

    @Autowired
    public UserAuthController(UserAuthService userAuthService) {
        this.userAuthService = userAuthService;
    }

    @GetMapping("/register")
    public void registerReturn(Model model)
    {
        userAuthService.registerReturn(model);
    }


    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public String registerUser(@ModelAttribute("user") UserDto userDto, BindingResult result, Model model) {
        if(userAuthService.emailExists(userDto.getEmail())) {
            result.rejectValue("email", "Error", "There is already an account registered with that email");
        }
        if(result.hasErrors()) {
            model.addAttribute("user", userDto);
            registerReturn(model);
            return "register";
        }
        userAuthService.registerUser(userDto);
        return "redirect:/login";
    }

}
