package pl.edu.agh.kis.erudite.loan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class LoanService {
    private final LoanRepository loanRepository;

    @Autowired
    public LoanService(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    public Iterable<Loan> getAllLoans() {
        return loanRepository.findAll();
    }

    public Loan getLoan(Long id) {
        return loanRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void createLoan(Loan loan) {
        loanRepository.save(loan);
    }
    public void deleteLoan(Long id) {
        if(!loanRepository.existsById(id)) {
            throw new ItemNotFoundException(id);
        }
        loanRepository.deleteById(id);
    }
}
