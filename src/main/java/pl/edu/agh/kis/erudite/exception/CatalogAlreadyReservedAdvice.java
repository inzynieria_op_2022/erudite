package pl.edu.agh.kis.erudite.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CatalogAlreadyReservedAdvice {
    @ExceptionHandler(CatalogAlreadyReservedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String catalogAlreadyReservedHandler(CatalogAlreadyReservedException e) {
        return e.getMessage();
    }
}
